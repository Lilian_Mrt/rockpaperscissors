function getComputerChoice(){
    let a = Math.floor(Math.random()*3);
    if (a ==0) {
        return "rock"
    } else if (a==1){
        return "paper"
    } else {
        return "scissors"
    }
}

function oneRound(playerSelection, computerSelection){
    if (playerSelection == "rock" && computerSelection == "scissors") {
      return "You win! Rock beats Scissors";
    } else if (playerSelection == "rock" && computerSelection == "paper") {
      return "You lose! Paper beats Rock";
    } else if (playerSelection == "rock" && computerSelection == "rock") {
      return "Draw!";
    } else if (playerSelection == "paper" && computerSelection == "scissors") {
      return "You lose! Scissors beats Paper";
    } else if (playerSelection == "paper" && computerSelection == "paper") {
      return "Draw!";
    } else if (playerSelection == "paper" && computerSelection == "rock") {
      return "You win! Paper beats Rock";
    } else if (playerSelection == "scissors" && computerSelection == "scissors") {
      return "Draw!";
    } else if (playerSelection=="scissors" && computerSelection == "paper") {
        return "You win! Scissors beats Paper"
    } else if (playerSelection=="scissors" && computerSelection == "rock") {
        return "You lose! Rock beats Scissors"
    }
}

// function game(){
//     let compteurWin = 0;
//     let compteurDraw = 0;
//     for (let i=0;i<5;i++){
//         let playerSelection = prompt("Rock, Paper or Scissors ?")
//         playerSelection = playerSelection.toLowerCase();
//         while (playerSelection!="rock" && playerSelection!="paper" && playerSelection!="scissors"){
//             playerSelection = prompt("Wrong propositon. Rock, Paper or Scissors?")
//         }
//         const computerSelection = getComputerChoice();
//         let ret = oneRound(playerSelection, computerSelection);
//         if (ret=="Draw!"){
//             compteurDraw++;
//         } else if (ret.charAt(4)=="w"){
//             compteurWin++;
//         }
//         console.log(ret);
//     }
//     console.log(`${compteurWin} à ${5-compteurDraw-compteurWin}`)
// }

const r = document.querySelector("#rock");
const p = document.querySelector("#paper");
const s = document.querySelector("#scissors");

let playerWin = 0;
let computerWin = 0;
const score = document.createElement("div");
const lastResult = document.createElement("div")
const body = document.querySelector("body");
body.appendChild(score);
body.appendChild(lastResult);

const restartButton = document.createElement("button");
restartButton.textContent = "Restart";

function abstractFunction(str) {
  let result = oneRound(str, getComputerChoice());
  if (result.charAt(4) == "w") {
    playerWin++;
    lastResult.textContent = "You won the round !"
  } else if (result.charAt(4) == "l") {
    computerWin++;
    lastResult.textContent = "You lost the round !";
  } else {
    lastResult.textContent = "It's a draw!";
  }
  score.textContent = `${playerWin} - ${computerWin}`;
  if (playerWin == 5){
    lastResult.textContent = "You won the game !";
    body.appendChild(restartButton);
    r.disabled = true;
    p.disabled = true;
    s.disabled = true;
  } else if (computerWin == 5){
    lastResult.textContent = "Computer wins the game !";
    body.appendChild(restartButton);
    r.disabled = true;
    p.disabled = true;
    s.disabled = true;
  }
}

r.addEventListener("click", () => abstractFunction("rock"));
p.addEventListener("click", () => abstractFunction("paper"));
s.addEventListener("click", () => abstractFunction("scissors"));
restartButton.addEventListener("click",() => location.reload(true))


